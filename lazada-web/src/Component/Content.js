import react, { Component } from 'react'
import {Row, Col, InputGroup, Form, Card, Carousel, Button, ListGroup } from 'react-bootstrap'
import './content.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Container from 'react-bootstrap/Container';
import img1 from '../image/1.jpeg'
import img2 from '../image/2.png'
import img3 from '../image/3.jpeg'
import img4 from '../image/4.jpeg'
import { BiSearchAlt2 } from 'react-icons/bi';



class Content extends Component{
    
    
    render(){
        var lazmall = [1,2,3,4,5,6]
        var categories = [1,2,3,4,5,6,7,8,9,10,11,12]
        var justforyou = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18]
        return(
            <Container>
                <div className="justify-content-center header" >
                    <Row>
                            <Col md="2">
                                <Form.Label className="title--text" >Lazada</Form.Label>
                            </Col>
                            <Col md="6">
                            <InputGroup className="mb-3" style={{marginTop : 20}}> 
                                <Form.Control
                                placeholder="Search in Lazada"
                                aria-label="Search in Lazada"
                                aria-describedby="basic-addon2"
                                />
                                <Button variant="outline-secondary" id="button-addon2">
                                <BiSearchAlt2 />
                                </Button>
                            </InputGroup>
                            </Col>
                            <Col md="3"><img
                                    className="d-block w-100"
                                    src={img4}/>
                            </Col>
                    </Row>
                </div>
                <Row className="justify-content-center content-1" >
                    <Col md="3">
                        <ListGroup>
                            <ListGroup.Item>categories 1</ListGroup.Item>
                            <ListGroup.Item>categories 2</ListGroup.Item>
                            <ListGroup.Item>categories 3</ListGroup.Item>
                            <ListGroup.Item>categories 4</ListGroup.Item>
                            <ListGroup.Item>categories 5</ListGroup.Item>
                            <ListGroup.Item>categories 6</ListGroup.Item>
                            <ListGroup.Item>categories 7</ListGroup.Item>
                            <ListGroup.Item>categories 8</ListGroup.Item>
                            <ListGroup.Item>categories 9</ListGroup.Item>
                        </ListGroup>
                    </Col>
                    <Col md="9">
                    <Carousel>
                    <Carousel.Item>
                        <img
                        className="d-block w-100"
                        src={img1}
                        alt="First slide"
                        />
                    </Carousel.Item>
                    <Carousel.Item>
                        <img
                        className="d-block w-100"
                        src={img1}
                        alt="First slide"
                        />
                    </Carousel.Item>
                    </Carousel>
                    </Col>
                </Row>
                <Row style={{marginTop : 10}}>
                    <Col md xs="6">
                        <Button className="button-content" variant="light"><BiSearchAlt2 /> LazMall</Button>
                    </Col>
                    <Col md xs="6">
                        <Button className="button-content" variant="light"><BiSearchAlt2 /> Vouchers</Button>
                    </Col>
                    <Col md xs="6"> 
                        <Button className="button-content" variant="light"><BiSearchAlt2 /> Top-Up, Bill&Fodd</Button>
                    </Col>
                    <Col md xs="6">
                        <Button className="button-content" variant="light"><BiSearchAlt2 /> 9Baht</Button>
                    </Col>
                    <Col md xs="6">
                        <Button className="button-content" variant="light"><BiSearchAlt2 /> LazPick</Button>
                    </Col>
                </Row>
                <Row style={{marginTop : 10}}>
                    <Col>
                        <h3 type="button">LasMall</h3>
                    </Col>
                    <Col style={{textAlign: "right"}}>
                        <h4 type="button" >Shop more</h4>
                    </Col>
                </Row>
                <Row>
                    {lazmall.map((item, key)=>{
                        return(
                            <Col md="2" xs="6">
                            <Card style={{ width: '12rem' }}>
                            <Card.Img variant="top" src={img2}/>
                            <Card.Body>
                            <Card.Title>item {item}</Card.Title>
                                <Card.Text>
                                detail item {item}
                                </Card.Text>
                            </Card.Body>
                            </Card>
                        </Col>
                        )
                    })}
                </Row>
                <Row style={{marginTop : 10}}>
                    <Col>
                        <h3 type="button">Categories</h3>
                    </Col>
                </Row>
                <Row>
                    {categories.map((item, key)=>{
                        return(
                            <Col md xs="6">
                            <Card style={{ width: '12rem' }}>
                            <Card.Img variant="top" src={img2}/>
                            <Card.Body>
                            <Card.Title>Categories {item}</Card.Title>
                            </Card.Body>
                            </Card>
                        </Col>
                        )
                    })}
                </Row>
                <Row style={{marginTop : 10}}>
                    <Col>
                        <h3 type="button">Just For You</h3>
                    </Col>
                </Row>
                <Row>
                    {justforyou.map((item, key)=>{
                        return(
                            <Col style={{marginBottom : 5}} md="2" xs="6">
                            <Card style={{ width: '12rem' }}>
                            <Card.Img variant="top" src={img2}/>
                            <Card.Body>
                            <Card.Title>Justforyou {item}</Card.Title>
                            <Card.Text>฿ 69.00</Card.Text>
                            </Card.Body>
                            </Card>
                        </Col>
                        )
                    })}
                </Row>
                <Row className="button-lodemore" style={{margin : 'auto', marginTop : 10}}>
                    <Button variant="outline-primary" > LOAD MORE</Button>
                </Row>
                <hr />
                <Row>
                        <Col style={{marginBottom : 5}}>
                            <Card style={{ width: '25rem' }}>
                            <Card.Img variant="top" src={img3}/>
                            </Card>
                        </Col>
                        <Col style={{marginBottom : 5}}>
                            <Card style={{ width: '25rem' }}>
                            <Card.Img variant="top" src={img3}/>
                            </Card>
                        </Col>
                        <Col style={{marginBottom : 5}}>
                            <Card style={{ width: '25rem' }}>
                            <Card.Img variant="top" src={img3}/>
                            </Card>
                        </Col>
                </Row>
            </Container>
            
        )
    }
}
export default Content;